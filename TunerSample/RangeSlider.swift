//
//  RangeSlider.swift
//  TunerSample
//
//  Created by George Romas on 29/07/2019.
//  Copyright © 2019 George Romas. All rights reserved.
//

import UIKit

class RangeSlider: UIView {

    private let leftDot = UIView(frame: .zero)
    private let rightDot = UIView(frame: .zero)
    private let sliderBackLine = UIView(frame: .zero)
    private let sliderRangeLine = UIView(frame: .zero)
    private let stack = UIStackView(frame: .zero)
    
    private var panGesture: UIPanGestureRecognizer!
    private var leftDotToLeft: NSLayoutConstraint!
    private var rightDotToRight: NSLayoutConstraint!
    
    public var mainColor = UIColor.red
    
    public private(set) var options: [String] = ["great", "small", "first", "second", "third"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialUISetup()
    }
    
    func initialUISetup() {
        
        // sliderBackLine >
        
        sliderBackLine.translatesAutoresizingMaskIntoConstraints = false
        sliderBackLine.backgroundColor = .black
        self.addSubview(sliderBackLine)
        
        sliderBackLine.addConstraint(
            NSLayoutConstraint(
                item: sliderBackLine, attribute: .height, relatedBy: .equal,
                toItem: nil, attribute: .notAnAttribute,
                multiplier: 1, constant: 4
            )
        )
        
        self.addConstraint(
            NSLayoutConstraint(
                item: sliderBackLine, attribute: .leadingMargin, relatedBy: .equal,
                toItem: self, attribute: .leadingMargin,
                multiplier: 1, constant: 0
            )
        )
        
        self.addConstraint(
            NSLayoutConstraint(
                item: sliderBackLine, attribute: .trailingMargin, relatedBy: .equal,
                toItem: self, attribute: .trailingMargin,
                multiplier: 1, constant: 0
            )
        )
        
        self.addConstraint(
            NSLayoutConstraint(
                item: sliderBackLine, attribute: .centerY, relatedBy: .equal,
                toItem: self, attribute: .centerY,
                multiplier: 1, constant: 0
            )
        )
        
        // sliderBackLine <
        
        // titles >
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.spacing = 5
        stack.distribution = .fillEqually
        self.addSubview(stack)
        
        self.addConstraint(
            NSLayoutConstraint(
                item: stack, attribute: .left, relatedBy: .equal,
                toItem: self, attribute: .left,
                multiplier: 1, constant: 0
            )
        )
        self.addConstraint(
            NSLayoutConstraint(
                item: stack, attribute: .right, relatedBy: .equal,
                toItem: self, attribute: .right,
                multiplier: 1, constant: 0
            )
        )
        
        self.addConstraint(
            NSLayoutConstraint(
                item: stack, attribute: .top, relatedBy: .equal,
                toItem: sliderBackLine, attribute: .bottom,
                multiplier: 1, constant: 20
            )
        )
        
        for (i, title) in options.enumerated() {
            
            let label = UILabel(frame: .zero)
            label.text = title
            label.textAlignment = .center
            label.textColor = .black
            label.font = .systemFont(ofSize: 17)
            label.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(label)
            
            stack.addArrangedSubview(label)
        }
        
        // titles <
        
        func setupDotView(_ dotView: UIView) {
            let dotRectSide: CGFloat = 28
            dotView.translatesAutoresizingMaskIntoConstraints = false
            dotView.backgroundColor = self.mainColor
            dotView.clipsToBounds = true
            dotView.layer.cornerRadius = dotRectSide/2
            self.addSubview(dotView)
            
            dotView.addConstraint(
                NSLayoutConstraint(
                    item: dotView, attribute: .height, relatedBy: .equal,
                    toItem: nil, attribute: .notAnAttribute,
                    multiplier: 1, constant: dotRectSide
                )
            )
            
            dotView.addConstraint(
                NSLayoutConstraint(
                    item: dotView, attribute: .width, relatedBy: .equal,
                    toItem: nil, attribute: .notAnAttribute,
                    multiplier: 1, constant: dotRectSide
                )
            )
            
            self.addConstraint(
                NSLayoutConstraint(
                    item: dotView, attribute: .centerY, relatedBy: .equal,
                    toItem: self, attribute: .centerY,
                    multiplier: 1, constant: 0
                )
            )
        }
        
        // leftDot >
        
        setupDotView(leftDot)
        
        self.leftDotToLeft = NSLayoutConstraint(
            item: leftDot, attribute: .left, relatedBy: .equal,
            toItem: self, attribute: .left,
            multiplier: 1, constant: 20
        )
        self.addConstraint(self.leftDotToLeft)
        self.leftDot.backgroundColor = .blue
        
        // leftDot <
        
        // rightDot >
        
        setupDotView(rightDot)
        
        self.rightDotToRight = NSLayoutConstraint(
            item: rightDot, attribute: .right, relatedBy: .equal,
            toItem: self, attribute: .right,
            multiplier: 1, constant: -20
        )
        self.addConstraint(self.rightDotToRight)
        
        // rightDot <
        
        // sliderRangeLine >
        
        sliderRangeLine.translatesAutoresizingMaskIntoConstraints = false
        sliderRangeLine.backgroundColor = .red
        self.insertSubview(sliderRangeLine, aboveSubview: sliderBackLine)
        
        self.addConstraint(
            NSLayoutConstraint(
                item: sliderRangeLine, attribute: .top, relatedBy: .equal,
                toItem: sliderBackLine, attribute: .top,
                multiplier: 1, constant: 0
            )
        )
        self.addConstraint(
            NSLayoutConstraint(
                item: sliderRangeLine, attribute: .bottom, relatedBy: .equal,
                toItem: sliderBackLine, attribute: .bottom,
                multiplier: 1, constant: 0
            )
        )
        self.addConstraint(
            NSLayoutConstraint(
                item: sliderRangeLine, attribute: .leading, relatedBy: .equal,
                toItem: leftDot, attribute: .centerX,
                multiplier: 1, constant: 0
            )
        )
        self.addConstraint(
            NSLayoutConstraint(
                item: sliderRangeLine, attribute: .trailing, relatedBy: .equal,
                toItem: rightDot, attribute: .centerX,
                multiplier: 1, constant: 0
            )
        )
        
        // sliderRangeLine <
        
        self.layoutIfNeeded()
        
        // Gesture
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(gesture:)))
        self.addGestureRecognizer(panGesture)
    }
    
    struct ActiveDot {
        enum LimitsType {
            case leftDot
            case rightDot
        }
        
        let view: UIView
        let limitsType: LimitsType
    }
    
    var activeDot: ActiveDot?

    @objc func panGestureAction(gesture: UIPanGestureRecognizer) {
        let state = gesture.state
        let location = gesture.location(in: self)
        let translation = gesture.translation(in: self)
        
        if state == .began {
            print(location)
            
            if leftDot.frame.contains(location) {
                activeDot = ActiveDot(view: self.leftDot, limitsType: .leftDot)
            } else if rightDot.frame.contains(location) {
                activeDot = ActiveDot(view: self.rightDot, limitsType: .rightDot)
            }
        }
        
        if state == .changed {
            if let activeDot = self.activeDot {
                self.setNeedsLayout()
                
                if activeDot.limitsType == .leftDot {
                    var newConstrainConstan = self.leftDotToLeft.constant + translation.x
                    let leftLimit: CGFloat = 20
                    let rightLimit = self.rightDot.frame.origin.x - self.leftDot.frame.width
                    if newConstrainConstan < 20 { newConstrainConstan = leftLimit }
                    if newConstrainConstan > rightLimit { newConstrainConstan = rightLimit }
                    
                    self.leftDotToLeft.constant = newConstrainConstan
                    
                    gesture.setTranslation(.zero, in: self)
                }
                
                if activeDot.limitsType == .rightDot {
                    var newConstrainConstan = self.rightDotToRight.constant + translation.x
                    
                    let leftLimit = -(self.frame.width - (self.leftDot.frame.width + self.leftDot.frame.origin.x + self.rightDot.frame.width))
                    let rightLimit: CGFloat = -20
                    if newConstrainConstan < leftLimit { newConstrainConstan = leftLimit }
                    if newConstrainConstan > rightLimit { newConstrainConstan = rightLimit }
                    
                    print("newConstrainConstan right:", newConstrainConstan, leftLimit, (newConstrainConstan < leftLimit), self.leftDot.frame.maxX)
                    
                    self.rightDotToRight.constant = newConstrainConstan
                    
                    gesture.setTranslation(.zero, in: self)
                }
                
                
                
                //activeDot.view.center.x = newDotLocation
                
                self.layoutIfNeeded()
            }
        }
        
        if state == .cancelled || state == .ended || state == .failed {
            activeDot = nil
        }
    
    }

}
