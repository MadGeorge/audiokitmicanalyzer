//
//  ViewController.swift
//  TunerSample
//
//  Created by George Romas on 18/07/2019.
//  Copyright © 2019 George Romas. All rights reserved.
//

import UIKit
import AudioKit

class ViewController: UIViewController {

    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var freqLabel: UILabel!
    @IBOutlet weak var ampLabel: UILabel!
    
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    
    /**
        For standard tuning, the frequencies are:
        E2=82.41Hz
        A2=110Hz
        D3=146.8Hz
        G3=196Hz
        B3=246.9Hz
        E4=329.6Hz
    */
    
    //let noteNamesWithFlats = ["C", "D", "E", "F", "G", "A", "B"]
    //let noteFrequencies = [16.35, 18.35, 20.6, 21.83, 24.5, 27.5, 30.87]
    let noteFrequencies = [16.35, 17.32, 18.35, 19.45, 20.6, 21.83, 23.12, 24.5, 25.96, 27.5, 29.14, 30.87]
    let noteNamesWithSharps = ["C", "C♯", "D", "D♯", "E", "F", "F♯", "G", "G♯", "A", "A♯", "B"]
    let noteNamesWithFlats = ["C", "D♭", "D", "E♭", "E", "F", "G♭", "G", "A♭", "A", "B♭", "B"]
    
    let tasks = [16.35, 18.35, 20.6, 21.83, 24.5, 27.5, 30.87]
    var currentTask = -1
    var currentTaskNoteName = "C"
    
    private var mic: AKMicrophone!
    private var tracker: AKFrequencyTracker!
    private var silence: AKBooster!
    
    private var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AKSettings.audioInputEnabled = true
        mic = AKMicrophone()
        tracker = AKFrequencyTracker(mic)
        silence = AKBooster(tracker, gain: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AudioKit.output = silence
        do {
            try AudioKit.start()
            
            showCurrentTask()
            
            self.timer = Timer.scheduledTimer(timeInterval: 0.1,
                                 target: self,
                                 selector: #selector(ViewController.tic),
                                 userInfo: nil,
                                 repeats: true)
            
            
        } catch let e {
            AKLog("AudioKit.start() failed", e)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.timer?.invalidate()
        
        do {
            try AudioKit.stop()
        } catch let e {
            print("AudioKit.stop() failed", e)
        }
    }
    
    func showCurrentTask() {
        resultLabel.text = "..."
        
        currentTask = currentTask + 1
        if currentTask >= tasks.count {
            currentTask = 0
        }
        let task = tasks[currentTask]
        
        var taskFrequency = -1.0
        
        for (i, freq) in noteFrequencies.enumerated() {
            if freq == task {
                taskFrequency = freq
                currentTaskNoteName = noteNamesWithFlats[i]
                break
            }
        }
        
        if taskFrequency > 0 {
            taskLabel.text = currentTaskNoteName
        } else {
            fatalError("showCurrentTask: Unexpected error! taskFrequency is invalid!")
        }
    }
    
    @objc func tic() {
        let amplitudeAtTic = tracker.amplitude
        let ampString = String(format: "%0.2f", amplitudeAtTic)
        
        if amplitudeAtTic > 0.03 {
            freqLabel.text = String(format: "%0.1f", tracker.frequency)
            
            var frequency = Float(tracker.frequency)
            while frequency > Float(noteFrequencies[noteFrequencies.count - 1]) {
                frequency /= 2.0
            }
            while frequency < Float(noteFrequencies[0]) {
                frequency *= 2.0
            }
            
            var minDistance: Float = 10_000.0
            var index = 0
            
            for i in 0..<noteFrequencies.count {
                let distance = fabsf(Float(noteFrequencies[i]) - frequency)
                if distance < minDistance {
                    index = i
                    minDistance = distance
                }
            }
            let octave = Int(log2f(Float(tracker.frequency) / frequency))
            
            let noteNameWithSharps = noteNamesWithSharps[index]
            let noteNameWithFlats = noteNamesWithFlats[index]
            
            
            
            noteLabel.text = "\(noteNameWithSharps) | \(noteNameWithFlats) | \(octave) | amp\(ampString)"
            
            var resultText = "Incorrect!"
            if noteNameWithSharps == currentTaskNoteName {
                resultText = "Correct!"
                
                Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { [weak self] timer in
                    self?.showCurrentTask()
                }
            }
            
            resultLabel.text = resultText
            
        }
        ampLabel.text = ampString
    }


}

